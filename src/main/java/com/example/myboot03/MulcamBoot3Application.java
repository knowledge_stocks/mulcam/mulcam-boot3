package com.example.myboot03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MulcamBoot3Application {

	public static void main(String[] args) {
		SpringApplication.run(MulcamBoot3Application.class, args);
	}

}
